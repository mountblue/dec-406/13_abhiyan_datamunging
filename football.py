'''
Name:football.py
Description: Football class to find the difference between the For Goals and Aganist goal and show the minimum goal difference
Input:A .dat file having football dat
Output: The minimum goal difference teamname and their corresponding difference value
author:abhiyan timilsina  
'''


from datamunging.DatFileParser import DatFileParser
from datamunging.DataAnalyzer import DataAnalyzer 


class Football(DatFileParser, DataAnalyzer):
    # Call the base class constructors
    def __init__(self, file_path, for_goal_column, aganist_column, title):
        self.football_data = []
        super(Football, self).__init__(file_path, title, for_goal_column, aganist_column)
        self.football_data = self.getColumnsData()
    
    # Find Minimim Goal Difference
    def find_minimim_goal_diff(self):
        minimum_goal_diff = self.find_difference_columns(self.football_data)
        print(f''' The minimim difference is for team  {minimum_goal_diff['title']} with {minimum_goal_diff['value']} goal difference''')

# Creating a football class instance and finding the minimum goal
f = Football('./files/football.dat', 'F', 'A', 'Team').find_minimim_goal_diff()              
