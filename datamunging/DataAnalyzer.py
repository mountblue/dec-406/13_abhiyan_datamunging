''' 
Name:DataAnalyzer.py
Description:Find the difference between column
Input: A list of coulumns 
Output: The minimum difference column
author: abhiyan timilsina
'''


class DataAnalyzer():
    def find_difference_columns(self, columns_list):
        minimum_diff_title = -1
        minimum_diff_value = -1
        title = 0
        for entry in columns_list:
            min_diffrence = abs(entry[1] - entry[2])
            if minimum_diff_title is -1:
                minimum_diff_title = entry[title]
                minimum_diff_value = min_diffrence
            if min_diffrence < minimum_diff_value:
                minimum_diff_value = min_diffrence
                minimum_diff_title = entry[title]
        return {'title': minimum_diff_title, 'value': minimum_diff_value}     

