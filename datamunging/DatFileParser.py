''' 
Name:DatFileParser.py
Description:Contains a class to convert .dat file to a python dict
Input: A file having .dat extension
Output: The parsed dat file in dict form
author: abhiyan timilsina
'''

import re


class DatFileParser():
    # Initializing file paths 
    def __init__(self, dat_file_path, title, column1, column2):
        self.title = title
        self.column1 = column1
        self.column2 = column2
        self.dat_file_path = dat_file_path
        self.column_names = []
        self.column_dicts = []
   
    # Getting Column Names
    def find_column_names(self,  header_row):
        column_header_position = []
        column_expression = re.compile(r'\s*[\w\d]+[\n\s]{1}')
        self.column_names = list(map(lambda x: x.strip(), column_expression.findall(header_row)))
        for column_name in self.column_names:
            column_header_position.append(header_row.find(column_name))  
        return column_header_position
   
    # Reading column data and forming dict out of it 
    def getColumnsData(self):
        line_count = 0
        columns_start_indexes = []
        with open(self.dat_file_path) as dat_file:
            for row in dat_file:
                if line_count == 0:
                    columns_start_indexes = self.find_column_names(row)
                else:
                    try:
                        column_values_dict = {}
                        for i in range(0, len(columns_start_indexes)):
                            column_values_dict[self.column_names[i]] = re.findall(r'[\s]*[\w]+[\W]{1}', row[columns_start_indexes[i]:])[0]
                        self.column_dicts.append(column_values_dict)
                    except IndexError:
                        continue                        
                line_count += 1
        return self.minDiffColumn(self.title, self.column1, self.column2)
   
    # Finding and returning the main diff column integers    
    def minDiffColumn(self, title, column1, column2):
        diff_data = []
        for column_data in self.column_dicts:
            column_data[column1] = re.sub(r'[\D]', '', column_data[column1])
            column_data[column2] = re.sub(r'[\D]', '', column_data[column2])
            diff_data.append((column_data[title], int(column_data[column1]),int(column_data[column2])))
        return diff_data

