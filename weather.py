''' 
Name:weather.py
Description:A Weather class to find the minimum tempreture spread
Input: A weather in .dat format
Output: The minimum tempreture spread
author: abhiyan timilsina
'''


from datamunging.DatFileParser import DatFileParser
from datamunging.DataAnalyzer import DataAnalyzer 


class Weather(DatFileParser, DataAnalyzer):
    
    # Initializing base class constructors
    def __init__(self, file_path, mxt_column, mnt_column, title):
        super(Weather, self).__init__(file_path, title, mxt_column, mnt_column)
        self.weather_data = self.getColumnsData()
    
    # Finding the minimum tempreture spread    
    def find_minimim_spread(self):
        min_max_diff = self.find_difference_columns(self.weather_data)
        print(f''' The minimim difference is on data {min_max_diff['title']} with value of {min_max_diff['value']}''')

# Instantiate new Weather Class and file the Spread
w = Weather('./files/weather.dat', 'MxT', 'MnT', 'Dy').find_minimim_spread()

